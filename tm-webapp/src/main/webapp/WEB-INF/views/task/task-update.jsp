<jsp:include page="../include/_header.jsp" />

<h1>TASK UPDATE</h1>

<form action="/tasks/update/${task.id}" method="POST">
    <p>
        <div>NAME:</div>
        <div><input type="text" name="name" value="${task.name}"></div>
    </p>
    <p>
        <div>DESCRIPTION:</div>
        <div><input type="text" name="description" value="${task.description}"></div>
    </p>
    <button type="submit">SAVE TASK</button>
</form:form>

<jsp:include page="../include/_footer.jsp" />