<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp" />

<h1>TASK LIST</h1>

<table width="100%" border="1" cellpadding="10" style="border-collapse: collapse">
    <tr>
        <th width="200" nowrap="nowrap">ID</th>
        <th width="200" nowrap="nowrap">NAME</th>
        <th width="100%" align="left">DESCRIPTION</th>
        <th width="100" nowrap="nowrap">EDIT</th>
        <th width="100" nowrap="nowrap">DELETE</th>
    </tr>
    <c:forEach items="${tasks}" var="task">
            <tr>
                <td class="td_list"><c:out value="${task.id}"/></td>
                <td class="td_list"><a href="/tasks/view/${task.id}"><c:out value="${task.name}"/></a></td>
                <td class="td_list"><c:out value="${task.description}"/></td>
                <td style="font-size: 15px" align="center"><a href="/tasks/update/${task.id}">UPDATE</a></td>
                <td style="font-size: 15px" align="center"><a href="/tasks/delete/${task.id}">DELETE</a></td>
            </tr>
        </c:forEach>
</table>

<form action="/tasks/create" style="margin-top: 10px;">
    <button border="1" style="border-radius: 0">CREATE TASK</button>
</form>

<jsp:include page="../include/_footer.jsp" />