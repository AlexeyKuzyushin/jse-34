<jsp:include page="../include/_header.jsp" />

<h1>TASK INFO</h1>
<p>
    <div>ID</div>
    <div><input type="text" readonly="true" name="id" value="${task.id}"></div>
</p>
<p>
    <div>NAME</div>
    <div><input type="text" readonly="true" name="name" value="${task.name}"></div>
</p>
<p>
    <div>DESCRIPTION</div>
    <div><input type="text" readonly="true" name="description" value="${task.description}"></div>
</p>
<p>
    <div>CREATION-TIME</div>
    <div><input type="text" readonly="true" name="creationTime" value="${task.creationTime}"></div>
</p>
<p>
    <div>START-DATE</div>
    <div><input type="text" readonly="true" name="startDate" value="${task.startDate}"></div>
</p>
<p>
    <div>FINISH-DATE</div>
    <div><input type="text" readonly="true" name="finishDate" value="${task.finishDate}"></div>
</p>
<p>
    <div>PROJECT</div>
    <div><input type="text" readonly="true" name="project" value="${task.project.name}"></div>
</p>
<p>
    <div>USER</div>
    <div><input type="text" readonly="true" name="user" value="${task.user.login}"></div>
</p>
<jsp:include page="../include/_footer.jsp" />