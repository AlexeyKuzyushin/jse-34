<jsp:include page="../include/_header.jsp" />

<h1>PROJECT INFO</h1>
<p>
    <div>ID</div>
    <div><input type="text" readonly="true" name="id" value="${project.id}"></div>
</p>
<p>
    <div>NAME</div>
    <div><input type="text" readonly="true" name="name" value="${project.name}"></div>
</p>
<p>
    <div>DESCRIPTION</div>
    <div><input type="text" readonly="true" name="description" value="${project.description}"></div>
</p>
<p>
    <div>CREATION-TIME</div>
    <div><input type="text" readonly="true" name="creationTime" value="${project.creationTime}"></div>
</p>
<p>
    <div>START-DATE</div>
    <div><input type="text" readonly="true" name="startDate" value="${project.startDate}"></div>
</p>
<p>
    <div>FINISH-DATE</div>
    <div><input type="text" readonly="true" name="finishDate" value="${project.finishDate}"></div>
</p>
<p>
    <div>USER</div>
    <div><input type="text" readonly="true" name="user" value="${project.user.login}"></div>
</p>
<jsp:include page="../include/_footer.jsp" />