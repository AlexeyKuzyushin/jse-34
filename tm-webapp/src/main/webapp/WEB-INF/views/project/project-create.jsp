<jsp:include page="../include/_header.jsp" />
<h1>PROJECT CREATE</h1>
<form action="/projects/create" method="POST">
    <p>
        <div>USER-ID</div>
        <div><input type="text" name="userId" value="${userId}"></div>
    </p>
    <p>
        <div>NAME</div>
        <div><input type="text" name="name" value="${project.name}"></div>
    </p>
    <p>
        <div>DESCRIPTION</div>
        <div><input type="text" name="description" value="${project.description}"></div>
    </p>
    <button type="submit">SAVE PROJECT</button>
</form>
<jsp:include page="../include/_footer.jsp" />