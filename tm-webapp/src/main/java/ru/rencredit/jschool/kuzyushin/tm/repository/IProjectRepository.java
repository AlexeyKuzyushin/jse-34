package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

import javax.transaction.Transactional;
import java.util.List;

public interface IProjectRepository extends JpaRepository<Project, String> {

    List<Project> findAllByUserId(@NotNull String userId);

    Project findByUserIdAndId(@NotNull String userId, @NotNull String id);

    Project findByUserIdAndName(@NotNull String userId, @NotNull String name);

    @Transactional
    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Transactional
    void deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

    void deleteAllByUserId(@NotNull String userId);
}
