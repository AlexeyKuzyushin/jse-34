package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends JpaRepository<Task, String> {

    Task findByUserIdAndId(@NotNull String userId, @NotNull String id);

    Task findByUserIdAndName(@NotNull String userId, @NotNull String name);

    List<Task> findAllByUserId(@NotNull String userId);

    List<Task> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

    void deleteAllByUserId(@NotNull String userId);

    void deleteAllByProjectId(@NotNull String projectId);
}
