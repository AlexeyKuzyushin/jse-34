package ru.rencredit.jschool.kuzyushin.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectDTO extends AbstractEntityDTO {

    public static final long serialVersionUID = 1;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private String userId;

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @Nullable
    private Date creationDate;

    @Override
    public String toString() { return getName() + ": " + name;}

    @Nullable
    public static ProjectDTO toDTO(final @Nullable Project project) {
        if (project == null) return null;
        return new ProjectDTO(project);
    }

    @NotNull
    public static List<ProjectDTO> toDTO(final @Nullable List<Project> projects) {
        if (projects == null || projects.isEmpty()) return Collections.emptyList();
        @NotNull final List<ProjectDTO> result = new ArrayList<>();
        for (@Nullable final Project project: projects) {
            if (project == null) continue;
            result.add(new ProjectDTO(project));
        }
        return result;
    }

    public ProjectDTO(final @Nullable Project project) {
        if (project == null) return;
        id = project.getId();
        name = project.getName();
        description = project.getDescription();
        userId = project.getUser().getId();
        if (project.getCreationTime() != null) creationDate = project.getCreationTime();
        if (project.getStartDate() != null) startDate = project.getStartDate();
        if (project.getFinishDate() != null) finishDate = project.getFinishDate();
    }
}
