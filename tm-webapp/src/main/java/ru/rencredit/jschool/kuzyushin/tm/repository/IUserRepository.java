package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;

import javax.transaction.Transactional;

public interface IUserRepository extends JpaRepository<User, String> {

    User findByLogin(@NotNull String login);

    @Transactional
    void deleteByLogin(@NotNull String login);
}
